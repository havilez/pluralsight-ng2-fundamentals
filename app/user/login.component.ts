import { Component } from '@angular/core';
import { FormsModule, NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService }  from './auth.service';

@Component({
    templateUrl: 'app/user/login.component.html',
    styles: [`
        em {float:right; color:#E05C05; padding-left:10px;}
    `]
  })



export class LoginComponent {

    userName :string;
    loginInvalid = false;

    // what is equivalent if defining property on class i.e injecting library into class???
    constructor(private authService:AuthService, private router:Router) {

    }

    login( formValues ) {
        this.authService.loginUser( formValues.userName,
            formValues.password ).subscribe( resp => {
                if ( !resp ) {
                    this.loginInvalid = true;
                } else {
                    this.router.navigate(['events']);
                }
            });

       
    }

    cancel() {
        // got to events list page w/o login user
        this.router.navigate(['/events']);
    }
}