import { Component, OnInit, Inject } from '@angular/core';
import  { Router }  from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService  } from './auth.service';
import { TOASTR_TOKEN , Toastr} from '../common/toastr.service'

@Component({
 templateUrl : 'app/user/profile.component.html',
 styles: [ `
    em {float:right; color:#E05C05; padding-left:10px;}
    .error input  { background-color: #E3C3C5;}
    .error ::-webkit-input-placeholder {color: #999; }
 `]
})
export class ProfileComponent implements OnInit {

   public profileForm: FormGroup;
  public firstName: FormControl;
  public lastName: FormControl;

  // inject AuthService and Router
  constructor(private authService: AuthService,
              private router :Router,
              @Inject(TOASTR_TOKEN) private toastr: Toastr) {

  }

  ngOnInit() {
    this.firstName = new FormControl(this.authService.currentUser.firstName, [Validators.required, Validators.pattern('[a-zA-Z].*')]);
    this.lastName = new FormControl(this.authService.currentUser.lastName, Validators.required);
    
    // add controls to a form
    this.profileForm = new FormGroup({
         firstName: this.firstName,
         lastName : this.lastName
    });
  
 
  }

  cancel() {
    this.router.navigate(['events']);
  }

  saveProfile(formValues){
    if ( this.profileForm.valid){
      this.authService.updateCurrentUser(formValues.firstName, formValues.lastName)
        .subscribe(()  => { // ignore data returned from updateCurrent User
          this.toastr.success('Profile Saved');
        });
      // this.router.navigate(['events']);
   
    }
  }

  logout() {
    this.authService.logout().subscribe(() => {
      this.router.navigate(['/user/login']);
    });
  }

  validateLastName() {
   return this.lastName.valid || this.lastName.untouched
  }

  validateFirstName() {
    return this.firstName.valid || this.firstName.untouched
   }
 
  
       
}