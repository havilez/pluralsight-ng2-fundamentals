import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { IUser } from './user.model';


// this service keeps track of the current user
@Injectable()
export class AuthService {
    currentUser :IUser;

    constructor(private http :Http) {}

    loginUser( userName :string, password :string) {
        // should go to server to get data from DB, hard code for now
        // this.currentUser = {
        //     id: 1,
        //     userName: userName,
        //     firstName: 'Harold',
        //     lastName: 'Avilez'
        // }

        let headers = new Headers({'Content-Type' : 'application/json'});
        let options = new RequestOptions({headers :headers});
        let loginInfo = { username: userName, password :password};

        return this.http.post('/api/login',JSON.stringify(loginInfo),options)
        .do(response => {
            if (response ) {
                this.currentUser = <IUser>response.json().user;
            }
        }).catch( error => {
            return Observable.of(false);
        })

    }

    logout() {
        // clear out user on client
        this.currentUser = undefined;

        // logout user on server
        let headers = new Headers({'Content-Type' : 'application/json'});
        let options = new RequestOptions({headers :headers});
       

        return this.http.post('/api/logout',JSON.stringify({}), options);
    }

    isAuthenticated() {
        return !!this.currentUser;
    }

    checkAuthenticationStatus() {
        return this.http.get('/api/currentIdentity').map((response :any) => {
            if (response._body) {
                return response.json();
            } else  {
                return {}
            }
        })
        .do( currentUser => {
            // either  user object or empty object is returned by http call
            if ( !! currentUser.userName ) {
                this.currentUser = currentUser;
            }
        })
        .subscribe();
    }

    updateCurrentUser(firstName, lastName){
         this.currentUser.firstName = firstName;
         this.currentUser.lastName = lastName;

        // update current user on the server
        let headers = new Headers({'Content-Type' : 'application/json'});
        let options = new RequestOptions({headers :headers});
       
        return this.http.put(`/api/users/${this.currentUser.id}`, JSON.stringify(this.currentUser), options);




    }
}