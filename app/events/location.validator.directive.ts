import { Directive } from '@angular/core';
import { FormGroup, Validator, FormControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[validateLocation]',
    // add/register LocationValidator to list of services in NG_VALIDATORS
    providers: [{ provide: NG_VALIDATORS, 
                  useExisting: LocationValidator, 
                  multi: true 
                }]

})

export class LocationValidator implements Validator {
    validate(formGroup :FormGroup) : { [key :string ] :any } {
        let addressControl = formGroup.controls['address'];
        let cityControl = formGroup.controls['city'];
        let countryControl = formGroup.controls['country'];
        // go up a level in DOM to get to online url control
        let onlineUrlControl = (<FormGroup>formGroup.root).controls['onlineUrl'];

        if ((addressControl && addressControl.value )  && 
            (cityControl && cityControl.value ) &&
            (countryControl && countryControl.value ) ||
            ((onlineUrlControl && onlineUrlControl.value) ) )  {
                return null;
        } else {
            return {validateLocation: false}
        }


    }
}