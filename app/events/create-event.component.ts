import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { EventService } from './shared/event.service'


@Component({
    selector: 'create-event',
    // FIX-ME: using templateUrl cause conflict with other event routes
    // I had this problem and my problem was the templateURL didn't match the file name for the HTML file.
    // 
    templateUrl: 'app/events/create-event.component.html',
    // template: `
    // <h1>New Event</h1>
    // <hr>
    // <div class = "col-md-6">
    //     <h3> [Create Event Form will go here ] </h3>
    //     <br/>
    //     <br/>
    //     <button type="submit" class="btn btn-primary">Save </button>
    //     <button type="button"  class="btn btn-default" (click)="cancel()">Cancel</button>
    // </div>
    // `,
    styles: [`
    em {float:right; color:#E05C05; padding-left:10px;}
    .error input  { background-color: #E3C3C5;}
    .error ::-webkit-input-placeholder {color: #999; }
    `]
})

export class CreateEventComponent {
    // private router:Router;
    isDirty :boolean = true;

    constructor( private router: Router, 
                private eventService: EventService){

    }

    cancel() {
     //   this.router.navigateByUrl("/events");
        this.router.navigate(['/events']);
    }

    saveEvent( formValues ) {
        // console.log(formValues);
        // event service now returns an obsevable
        // no data is returned until you subscribe to the obsrvable
        this.eventService.saveEvent(formValues).subscribe( event => {
            this.isDirty = false;
            this.router.navigate(['/events']);
        })
    
    }
}