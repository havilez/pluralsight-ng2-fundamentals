import  { TestBed, async, ComponentFixture} from '@angular/core/testing';
import { DebugElement }  from '@angular/core';
import { By } from '@angular/platform-browser';

import { SessionListComponent} from './session-list.component';

import { AuthService } from '../../user/auth.service';
import { VoterService } from './voter.service';
import { ISession } from '../shared/event.model';

