import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router'; //?

// import { EventService } from '../shared/event.service';
import { IEvent, EventService, ISession } from '../shared/index'



// no selector needed for this component because
// this compnent is not a child compnent of another page
// its going to be routed  to directly
@Component({
    // selector: 'session-list',
    templateUrl: 'app/events/event-details/event-details.component.html',
    styles: [`
        .container {
             padding-left: 20px; }; padding-right: 20px; 
            }
        .event-image { 
            height: 100px; 
        }
        a {cursor:pointer}
    `]
})

export class EventDetailsComponent implements OnInit {
    event:IEvent;
    addMode :boolean;
    filterBy :string ='all';
    sortBy :string = 'votes';

    constructor(private eventService : EventService,
     private route: ActivatedRoute) {
        
    }

    ngOnInit() {

        /** original synchronous call **/
        // this.route.params.forEach((params :Params) => {
        //  this.event = this.eventService.getEvent(+params['id']);
        //  this.addMode = false;
        //  })
      
        // //this.route.params.forEach((params :Params) => {
        this.route.data.forEach((data) => {

            // Now retreival of data is handled in resolve for getEvent route
            // "data" is passed from resolve to this function
            /// This does not work with routing to same component
            /// for example, when rooting to event via search box result
            /// this.event = this.route.snapshot.data['event'];
            this.event = data['event'];
            this.addMode = false;
             
             // // this is used with canActivate route activator
            // this.eventService.getEvent(+params['id']).subscribe((event :IEvent) => {
            //     this.event = event;
            //     this.addMode = false;
            // })

        })
      

    

      

     
        // this.event = this.eventService.getEvent(
            // Number(this.route.snapshot.params['id']) ); // note: 'id' param matches param name in route
    }

    addSession() {
        this.addMode = true;
    }

    saveNewSession(session:ISession){
        // add new id to new session
        const nextId = Math.max.apply(null, this.event.sessions.map(s => s.id));
        session.id = nextId + 1;

        this.event.sessions.push(session);
        // using saveEvent for updates, because server endpoint has been setup, such that
        // when an 'session id' is added in an existing event model it knows to update the event.
        this.eventService.saveEvent( this.event ).subscribe();

        // NOTE: this is not inside  subscribe call, we're assuming update will be successful
        this.addMode = false;

    }

    cancelAddSession() {
        this.addMode = false;
    }

}