import { Injectable }  from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ISession } from '../shared/event.model';



@Injectable()

export class VoterService {
    constructor(private http :Http) {

    }

    deleteVoter( eventId :number, session :ISession, voterName :string) {
        session.voters = session.voters.filter(voter => voter != voterName);

        // update coressponding server data
        let url = `/api/events/${eventId}/sessions/${session.id}/voters/${voterName}` ;
        
        // self subscription here is ok, instead of subscription from calling method
        // as we're just triggering action on server i.e. deleting data on server.
        //FIX-ME: what about return status of server call
       this.http.delete(url ).catch(this.handleError).subscribe();
    }

    addVoter( eventId :number,session :ISession, voterName :string) {
        // update client
        session.voters.push(voterName);

        // update corresponding server data
        let headers = new Headers({'Content-Type' : 'application/json'});
        let options = new RequestOptions({headers: headers});
        let url = `/api/events/${eventId}/sessions/${session.id}/voters/${voterName}` ;

        // self subscription here is ok, instead of subscription from calling method
        //  as we are not concerned about data returned from server
        // FIX-ME: what about return status from server
        this.http.post(url, JSON.stringify({}),options).catch(this.handleError).subscribe();



    }

    userHasVoted(session :ISession, voterName :string) {
        return session.voters.some(voter => voter === voterName)
    }

    private handleError( error :Response){
        return Observable.throw(error.statusText);
      }
}