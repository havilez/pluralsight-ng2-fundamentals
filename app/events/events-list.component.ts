import { Component , OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { EventService } from './shared/event.service';
import { IEvent } from './shared/event.model';



@Component({
    //N/A  selector : 'events-list', 
    // now router will specify path to this cmponent
    providers: [EventService],
    // templateUrl: 'app/events/events-list.component.html'
    template: `
        <div>
            <h1>Upcoming Angular 2 Events</h1>
            <hr>
            <div class="row">
           
            <!-- assign variable event to event array element ----->
             <!--*ngFor is a structural directive, it will add an HTML element to DOM,
                for each item in the events array in component
             -->
                <div *ngFor="let event of events"  class="col-md-5">
                <div >
                    <event-thumbnail [event]="event"> </event-thumbnail>          
              </div>
                    </div>
            </div>
        </div>
    `,
    styles: [`
      .thumbnail { min-height: 210px;}
      .pad-left { margin-left: 10px; }
      .well div { color: #bbb; }
    `]
})
export class EventsListComponent implements OnInit {

  events : IEvent[];
//    eventService :EventService;

  // dependency inject eventService into component
  constructor( private eventService: EventService ,
              private route:ActivatedRoute){

  }

  // called when component is loaded
  ngOnInit() {
    // this.events = this.eventService.getEvents();
    // simulate asynchronous http call to get event list
    //  this.eventService.getEvents().subscribe( events => { this.events = events})
 
    // now since eventlist is being passed in via the resolve no need to make call here
    // retrieve events data from the route, which was placed there via the resolve
    this.events = this.route.snapshot.data['events'];
}
    
    

}