import { Injectable }  from '@angular/core';
import { Resolve } from '@angular/router';

import { EventService } from '../shared/event.service'

@Injectable()
export class EventListResolver implements Resolve<any> {

    constructor(private eventService :EventService) {

    }


    resolve( ) {
        // make asynchronous call hare
        // in resolve, it needs to return an observable, hence call to map
        // http call for getEvents Observable, is not made until someone subscribes to the result
        // in this case the resolve function in Angular  subscribes to the Observable. How??

        return this.eventService.getEvents()

    }

}