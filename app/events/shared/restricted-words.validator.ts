import { FormControl } from '@angular/forms';

// make this a function that returns  a function, so that restricted word values can
    // be passed as parameters to  validator function
    
    // FIX-ME: if word is valid, returns an array item containing null, causining array.length to fail,
    // it should notstore value in the array at all
export function  restrictedWords( words ) {
        return ( control: FormControl ) : { [ key: string] :any } => {
            if (!words)
                return null;

            // var invalidWords = words.map(w => control.value.includes(w) ? w : null)
            var invalidWords = words.map (function(w){
                if (control.value.includes(w ))
                    return w
                else
                    return null
               
            })

            invalidWords.reduce()

            return invalidWords && invalidWords.length > 0
            ? {'restrictedWords' : invalidWords.join(', ')}
            : null
        }
    }