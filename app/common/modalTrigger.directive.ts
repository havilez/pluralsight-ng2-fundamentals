import { Directive, OnInit, Inject, ElementRef,Input } from '@angular/core';

import { JQ_TOKEN } from './jQuery.service';



@Directive({
    // put selector in brackets to indicate its a directive instead of an html tag
    selector: '[modal-trigger]'

})

export class ModalTriggerDirective {
    private el:HTMLElement;
    @Input('modal-Trigger') modalId: string;


    //ElementRef -> when this directive is constructed, access the Html element its on
    constructor(ref :ElementRef, @Inject(JQ_TOKEN) private $ :any ) {
    
        this.el = ref.nativeElement;
        
    }

    ngOnInit() {
        this.el.addEventListener('click', e => {
            this.$('#${this.modalId}').modal({})
        })
  
    }
}