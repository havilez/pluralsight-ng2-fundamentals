import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


// import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {
    EventsListComponent,
    EventThumbnailComponent,
    CreateEventComponent,
    EventService,
    EventDetailsComponent,
    EventRouteActivator,
    EventListResolver,
    SessionListComponent,
    CreateSessionComponent,
    UpvoteComponent,
    // VoterService,
    LocationValidator,
    DurationPipe,
    EventResolver




 } from './events/index';

//FIX-ME
import { VoterService } from './events/event-details/index'

import { EventsAppComponent } from  
     './events-app.component'



import { NavBarComponent }  from 
        './nav/navbar.component'

import { Error404Component }    from  
        './errors/404.component'

import { UserModule } from
        './user/user.module';

import { appRoutes } from  './routes';

// register here, so it can be used in components in multiple modules
import { AuthService } from './user/auth.service';

import { Toastr, 
        TOASTR_TOKEN ,
        JQ_TOKEN, 
        CollapsibleWellComponent, 
        SimpleModalComponent,
        ModalTriggerDirective
    } from './common/index';


declare let toastr :Toastr;
declare let jQuery :Object;

// angular module
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        UserModule,
        RouterModule.forRoot(appRoutes,{enableTracing: true})
        // NgbModule.forRoot()
    ],
    // register components here
    declarations: [
        EventsAppComponent,
       CreateEventComponent,
        EventsListComponent,
        EventThumbnailComponent,
        EventDetailsComponent,
        NavBarComponent,
        Error404Component,
        SessionListComponent,
        CreateSessionComponent,
        CollapsibleWellComponent,
        SimpleModalComponent,
        UpvoteComponent,
        ModalTriggerDirective,
        LocationValidator,
        DurationPipe
          
     
       
        ],
        // providers are shared across modules
    providers: [
        EventService, 
        {  provide: TOASTR_TOKEN, useValue: toastr},
        {   provide: JQ_TOKEN, useValue: jQuery  },
        //// long hand version of registering a class provider
        { provide: EventRouteActivator, 
          useClass: EventRouteActivator
        },
        EventListResolver,
        {
            provide: 'canDeactivateCreateEvent',
            useValue: checkDirtyState

        },
        EventResolver,
        AuthService,
        VoterService
    ],
    bootstrap: [EventsAppComponent]  // top app component

})

export class AppModule {}

// placed here only for example
function checkDirtyState(component:CreateEventComponent) {
    if (component.isDirty)
        return window.confirm('You have not saved this event, do you really want to cancel?')
    return true;
}