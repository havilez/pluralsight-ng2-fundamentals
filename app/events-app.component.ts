// this is the main app component at the root of the component tree


// using alias to module list in map section of systemjs.config.js
import { Component } from '@angular/core';

import { AuthService } from './user/auth.service';


// component decorator
// component contains a config object
// selector or html tag used by component
// added router-outlet component to this component
@Component({
   
    template: `
        <nav-bar> </nav-bar>
       <router-outlet></router-outlet> 
    `
})

// component class contains business logic and data used by the component
export class EventsAppComponent {
        
    constructor(private auth :AuthService) { }

    ngOnInit() {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this.auth.checkAuthenticationStatus();
    }

}