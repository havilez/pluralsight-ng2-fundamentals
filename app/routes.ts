import { Routes } from '@angular/router';

import {
    EventsListComponent,
    EventDetailsComponent,
    CreateEventComponent ,
    EventRouteActivator ,
    EventListResolver,
    EventResolver,
    CreateSessionComponent
} from './events/index'


import { Error404Component }    from 
    './errors/404.component';


// application routes are defined here
export const appRoutes:Routes = [
    // new path MUST be first so that angular can differtiate between
    // that route and  id route ???
     { path: 'events/new', component: CreateEventComponent ,canDeactivate: ['canDeactivateCreateEvent'] },
    
     // before resolving this route call the EventListResover, when that resolver finishes
    // and returns with some data, add this data to the route as a property
    // named events
     {  path: 'events', component: EventsListComponent , resolve: {events:EventListResolver}  },
    { path: 'events/:id', component: EventDetailsComponent, resolve: {event :EventResolver} },
    { path: 'events/session/new' , component: CreateSessionComponent },
    { path: '404', component: Error404Component },
   
    {  path: '',  redirectTo: '/events', pathMatch: 'full'},
    { path: 'user', loadChildren: 'app/user/user.module#UserModule'}
]